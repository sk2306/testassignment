<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
<!DOCTYPE html>
<html>
<style>
	.row {
	  	float: left;
	    width: 100%;
	}
	.block {
	  	float: left;
	    width: 300px;
	}
</style>
<body>
<div class="container">
<div style="margin-left: 400px;"><h5 style="color: red;"> ${error}</h5></div>
<div style="margin-left: 400px;"><h5 style="color: green;"> ${message}</h5></div>
  <h3>Hi <c:out value="${account.getAccountName()}"/></h3>
  <strong>Your Email</strong>: <c:out value="${account.getEmail()}"/><br>
  <strong>Additional Info</strong>: <c:out value="${account.getAdditionalInfo()}"/><br><br>
  <div class="row" >
  	<div class="block">
  		<img id="blah" style="margin-bottom: 5px;" 
  				src="${pageContext.servletContext.contextPath}/admin/getImage?accId=${account.getId()}"
  				height="111px" width="111px"/>
  		<form modelAttribute="account" action="uploadImage" method="post" enctype="multipart/form-data">
		    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		    <input type="file" multiple accept='image/*' style="margin-bottom: 5px;" id="imgInp" name="image"/>
		     <input class="btn btn-success" type="submit"  value="Upload"/>
		    <input type="hidden" name="id" value=<c:out value="${account.getId()}"/>>
		    <a type="button" class="btn btn-primary btn-success"
			               href="<c:url value="/admin/exportToXml?id=${account.getId()}"/>">Export</a>
			               <input id="importXMLFile" class="btn btn-success" type="submit" value="Import XML">
		    
		  </form>
  	</div>
	<%-- <div class="block" >
	  <strong>Update Account By XML</strong><br>
	  <form modelAttribute="account" action="uploadAccountXml" method="post" enctype="multipart/form-data">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	    <input type="hidden" name="id" value=<c:out value="${account.getId()}"/>>
	    <input id="fileXML" type="file" name="file"><br>
	    <input id="importXMLFile" class="btn btn-success" type="submit" value="Import XML">
	  </form>
	</div> --%>
	<%-- <div class="block" >
	  <strong>Export Account To XML</strong><br>
	  <a type="button" class="btn btn-primary btn-success"
			               href="<c:url value="/admin/exportToXml?id=${account.getId()}"/>">Export</a>
	</div> --%>
  </div>
  <strong>Available data sets</strong>:
  <h3>Available DataSets for current account </h3>
  <div>
    <a type="button" class="btn btn-success" href="<c:url value="/admin/add-watchlist?id=${account.getId()}"/>">Add New watchlist</a>
    <a type="button" class="btn btn-primary"
             			href="<c:url value="/admin/search"/>">Search</a>
  </div>
  <hr size="4" color="gray"/>

  <table class="table table-striped">
		<thead>
    		<tr>
    			<th>Data Set Name</th>
    			<th>Data Set Id</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody>
			    <c:forEach var="theWatch" items="${watchLists}">
			      <tr>
			        <td>${theWatch.getWatchListName()}  </td>
			        <td>${theWatch.getWatchListId()}   </td>
			
			        <td><a type="button" class="btn btn-primary"
			               href="<c:url value="/admin/view-watchlist?watchListId=${theWatch.watchListId}&accountId=${account.getId()}"/>">View</a>
			            <a type="button" class="btn btn-primary"
			               href="<c:url value="/delete-watchlist?watchListId=${theWatch.watchListId}&accountId=${account.getId()}"/>">Delete</a>
			        </td>
			      </tr>
			
			    </c:forEach>
			    <tr>
			    
		</tbody>
  </table>
  <br>
  
</div>
<script type="text/javascript">
	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	
	        reader.onload = function (e) {
	            $('#blah').attr('src', e.target.result);
	        }
	
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	
	$("#imgInp").change(function(){
	    readURL(this);
	});
</script>
<%@ include file="common/footer.jspf" %>
</body>
</html>