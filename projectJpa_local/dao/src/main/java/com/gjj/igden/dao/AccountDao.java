package com.gjj.igden.dao;

import java.io.InputStream;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.gjj.igden.model.Account;

public interface AccountDao {
  void setDataSource(DataSource dataSource);

  void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd);

  List<Account> getAllAccounts();

  boolean delete(Account account);

  boolean delete(Long id);

  Account getAccountById(Long id);

  boolean update(Account acc);

  boolean create(Account account);

  boolean setImage(Long accId, InputStream is);

  byte[] getImage(Long accId);
}
