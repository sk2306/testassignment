package com.gjj.igden.dao;

import java.util.List;

import com.gjj.igden.model.BaseModel;

public interface BaseDao {
    BaseModel save(BaseModel model);

    BaseModel findById(Long id);

    List<BaseModel> findAll();
}
