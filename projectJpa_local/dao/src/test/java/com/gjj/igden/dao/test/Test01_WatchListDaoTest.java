package com.gjj.igden.dao.test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.dao.daoimpl.AccountDaoImpl;
import com.gjj.igden.dao.daoimpl.RoleDaoImpl;
import com.gjj.igden.dao.daoimpl.WatchListDescDaoImpl;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.OperationParameters;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.model.WatchListDescKey;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPATestConfig.class })
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test01_WatchListDaoTest {

	private static final Logger logger = LoggerFactory.getLogger(Test01_WatchListDaoTest.class);

	@Autowired
	private WatchListDescDaoImpl watchListDescDao;

	@Autowired
	private AccountDaoImpl testAccountDaoImpl;
	@Autowired
	private RoleDaoImpl roleDaoImapl;

	private static WatchListDesc watchListDesc = null;
	private static Account account = null;
	private static final String ADMIN = "Admin_";

	
	@Before
	public void setup() {

		try {
			if(null == account) {
				if(roleDaoImapl.getCount() ==0) {
	    			roleDaoImapl.createDefaultRole();
	    		}
				account = getNewAccount(ADMIN + new Date().getTime());
				testAccountDaoImpl.create(account);
				logger.debug("Test01_WatchListDaoTest::Before creating account::" + account);
				account = testAccountDaoImpl.readByAccountName(account);
				logger.debug("Test01_WatchListDaoTest::After creating account::" + account);
				watchListDesc = getWatchListDesc(new Date().getTime());
				watchListDescDao.createWatchListDesc(watchListDesc);
				logger.debug("Test01_WatchListDaoTest::Before creating watchlist::" + watchListDesc);
				watchListDesc = watchListDescDao.read(watchListDesc);
				logger.debug("Test01_WatchListDaoTest::After creating Watchlist::" + watchListDesc);
			}
			
		} catch (DAOException e) {
			logger.error("Test01_WatchListDaoTest::Can't create account");
			e.printStackTrace();
		}
	}

	public WatchListDesc getWatchListDesc(Long watchListId) {
		WatchListDesc wl = new WatchListDesc();
		wl.setWatchListDescKey(new WatchListDescKey(watchListId, account.getId()));
		wl.setAccount(account);
		wl.setDataProviders("test data provider");
		wl.setMarketDataFrequency(10L);
		wl.setOperationParameterses(
				Arrays.asList(new OperationParameters(1L, "test1"), new OperationParameters(2L, "test2")));
		return wl;
	}

	private Account getNewAccount(String accountName) {
		Account account = new Account();
		account.setAccountName(accountName);
		account.setEmail(accountName + "@test.com");
		account.setAdditionalInfo("test my " + accountName);
		account.setPassword("123qwe");
		account.addRole(roleDaoImapl.readAll().get(0));
		return account;
	}

	@Test
	public void test01_addWatchListDesc() throws DAOException {
		boolean resultFlag = watchListDescDao.createWatchListDesc(getWatchListDesc(2L));
		logger.debug("Test01_WatchListDaoTest::resultFlag after adding watchlist desc::" + resultFlag);
		Assert.assertTrue(resultFlag);
	}

	@Test
	public void test02_readAll() throws Exception {
		List<IWatchListDesc> watchListDescs = watchListDescDao.readAll();
		logger.debug("Test01_WatchListDaoTest::Reading all datasets::" + watchListDescs);
		Assert.assertTrue(watchListDescs.size() > 0);
	}

	@Test
	public void test03_getDataSetsAttachedToAcc() {
		List<WatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(account.getId());
		logger.debug("Test01_WatchListDaoTest::Reading Data set list attached to account::" + account + dataSetList);
		final int expectedDataSetsAmount = 2;
		Assert.assertEquals(expectedDataSetsAmount, dataSetList.size());
	}

	@Test
	public void test04_createDataSet() throws Exception {
		WatchListDesc dataSet = watchListDescDao.read(watchListDesc);
		List<WatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(account.getId());
		dataSetList.forEach(p -> System.out.print(p.getId() + " ; "));
		int expectedDataSetsAmountAfterDeletion = 2;
		Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
		Assert.assertNotNull(dataSet);
		// dataSet.setWatchListId(111);
		dataSet.setWatchListName("just testing around");
		watchListDescDao.createWatchListDesc(dataSet);
		logger.debug("Test01_WatchListDaoTest::Data set after create::" + dataSet);
	}

	@Test
	public void test05_updateDesc() throws Exception {
		IWatchListDesc dataSet = watchListDescDao.read(watchListDesc);
		dataSet.setWatchListName("test update");
		logger.debug("Test01_WatchListDaoTest::Data set before update::" + dataSet);
		watchListDescDao.updateWatchListDesc(dataSet);
		final String dataSetNameDirect = watchListDescDao.read(watchListDesc).getWatchListName();
		logger.debug("Test01_WatchListDaoTest::Data set after update::" + watchListDescDao.read(watchListDesc));
		Assert.assertEquals("test update", dataSetNameDirect);
	}
	
	@Test
	public void test06_delete02() throws Exception {
		List<WatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(account.getId());
		final int expectedDataSetsAmount = 3;
		Assert.assertEquals(expectedDataSetsAmount, dataSetList.size());
		logger.debug("Test01_WatchListDaoTest::Data set before delete::" + dataSetList);
		boolean deleteResultFlag = watchListDescDao.deleteWatchList(dataSetList.get(0));
		dataSetList = watchListDescDao.getDataSetsAttachedToAcc(account.getId());
		logger.debug("Test01_WatchListDaoTest::Data set after delete::" + dataSetList);
		final int expectedDataSetsAmountAfterDeletion = 2;
		Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());

		if(null!= account) {
			testAccountDaoImpl.delete(account);
		}
	}
	
	/*private void tearDown() {
		try {
			if(null!= account) {
				testAccountDaoImpl.delete(account);
			}
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}*/
}